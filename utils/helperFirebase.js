
const { firestore } = require('../config/admin')

//Firestore query to get profiles that are matching the list uuid
async function getFirestoreProfiles(list) {
    if (list.length > 0) {
        const refs = list.map(el => firestore.doc(`users/${el._key}`))
        const users = await firestore.getAll(...refs)
        return users.map(doc => doc.data().profile)
    } else {
        return []
    }
}

module.exports = {
    getFirestoreProfiles
}