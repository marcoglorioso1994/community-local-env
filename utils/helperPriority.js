
module.exports = class HelperPriority {
    static findPriority(followers, following, totalLikes, totalComments) {
        //Gold influencer
        if (parseInt(followers) >= 1000000 && parseInt(following) >= 101 &&
            parseInt(totalComments) >= 91 && parseInt(totalLikes) >= 2001) {
            return 4;
        }
        //Silver influencer
        else if (parseInt(followers) >= 100000 && parseInt(following) >= 101 &&
            parseInt(totalComments) >= 61 && parseInt(totalLikes) >= 501) {
            return 3;
        }
        //Influencer
        else if (parseInt(followers) >= 10000 && parseInt(following) >= 101 &&
            parseInt(totalComments) >= 31 && parseInt(totalLikes) >= 201) {
            return 2;
        }
        //Advanced user
        else if (parseInt(followers) >= 6000 && parseInt(following) >= 101 &&
            parseInt(totalComments) >= 11 && parseInt(totalLikes) >= 51) {
            return 1;
        } else {
            //Basic user
            return 0;
        }
    }
}