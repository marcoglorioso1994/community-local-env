const axios = require("axios");

const basePathPosts = `https://europe-west1-dollapp-nodejs.cloudfunctions.net/posts`

/**
 * Http Helper Post
 *
 * @param {*} doc
 * @param {*} offset
 * @param {*} limit
 */
async function getAudiencePosts(user) {
    const url = `${basePathPosts}/${user}/audience`;

    try {
        const res = await axios.get(url)
        let data = res.data
        return data
    } catch (error) {
        console.log(error.response.data)
        return null
    }
}

module.exports = {
    getAudiencePosts
}