
module.exports = class Functions {

    static onlyUnique(value, index, self) {
        return self.indexOf(value) === index;
    }
}