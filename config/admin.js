const admin = require('firebase-admin');
var serviceAccount = require('../admin.json');

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "",
    storageBucket: "dollapp-nodejs.appspot.com",
    authDomain: "dollapp-nodejs.firebaseapp.com",
});

//Ref to firestore database
const firestore = admin.firestore();

module.exports = {
    firestore
}