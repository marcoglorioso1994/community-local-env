const arangojs = require("arangojs")

const arangoUrl = [""];

const db = new arangojs.Database({
    url: arangoUrl
});

db.useBasicAuth("", "");

const masterDB = new arangojs.Database({
    url: arangoUrl
});

masterDB.useBasicAuth("", "");
masterDB.useDatabase("");

module.exports = {
    db,
    masterDB
}