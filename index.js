const express = require('express');
var bodyParser = require('body-parser');
const cors = require('cors');
const swaggerUI = require("swagger-ui-express")
const { ValidationError } = require('express-validation')

const swagger = require("./swagger.json")
const communityAPI = require("./api/community")

const app = express();

app.use(bodyParser.json());// for parsing application/json

// Automatically allow cross-origin requests
app.use(cors({ origin: true }));

//swagger documentation endpoints
app.use("/api-docs", swaggerUI.serve, swaggerUI.setup(swagger));

//all garments endpoints
app.use(communityAPI);

app.use(function (err, req, res, next) {
    if (err instanceof ValidationError) {
        if (err.details.params) {
            console.log("ValidationError param: " + err.details.params[0].message)
            return res.status(400).send(err.details.params[0].message);
        }
        if (err.details.body) {
            console.log("ValidationError query: " + err.details.body[0].message)
            return res.status(400).send(err.details.body[0].message);
        }
        console.log(err)
        return res.status(400).json(err)
    }
    console.log(err.stack)
    return res.status(500).json(err)
})

// Expose Express API as a single Cloud Function:
exports.community = app;