const express = require("express");
const router = express.Router();

const ControllerRelations = require("./controllerRelations");
const Validator = require("./validator");

router.post("/",
  Validator.follow(),
  ControllerRelations.follow
);

router.delete("/:uuid/following/:toUUID",
  Validator.unFollow(),
  ControllerRelations.unFollow
);

module.exports = router;
