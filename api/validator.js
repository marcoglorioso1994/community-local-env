const Joi = require("joi");
const { validate } = require("express-validation");

module.exports = class Validator {
  //POST /nodes 
  static create() {
    return validate({
      body: Joi.object({
        uuid: Joi.string().min(20).max(40).regex(/^U\w{20,40}/).trim().strict().required(),
        name: Joi.string().required(),
        email: Joi.string().email().required(),
        priority: Joi.number().integer().min(0).max(5).optional(),
      }),
    });
  }

  //POST /relations
  static follow() {
    return validate({
      body: Joi.object({
        uuid: Joi.string().min(20).max(40).regex(/^U\w{20,40}/).trim().strict().required(),
        toUUID: Joi.string().disallow(Joi.ref('uuid')).min(20).max(40).regex(/^U\w{20,40}/).trim().strict().required()
      })
    });
  }

  //GET /nodes/{uuid}/following
  static getFollowing() {
    return validate({
      params: Joi.object({
        uuid: Joi.string().min(20).max(40).regex(/^U\w{20,40}/).trim().strict().required()
      }),
    });
  }

  //GET /nodes/{uuid}/followers  
  static getFollowers() {
    return validate({
      params: Joi.object({
        uuid: Joi.string().min(20).max(40).regex(/^U\w{20,40}/).trim().strict().required()
      }),
    });
  }

  //GET /nodes/{uuid}/following/{toUUID} 
  static isFollowing() {
    return validate({
      params: Joi.object({
        uuid: Joi.string().min(20).max(40).regex(/^U\w{20,40}/).trim().strict().required(),
        toUUID: Joi.string().disallow(Joi.ref('uuid')).min(20).max(40).regex(/^U\w{20,40}/).trim().strict().required()
      }),
    });
  }

  //GET /nodes/{uuid}/search?text=somewordswithspaces&limit=30 
  static search() {
    return validate({
      params: Joi.object({
        uuid: Joi.string().min(20).max(40).regex(/^U\w{20,40}/).trim().strict().required()
      }),
      query: Joi.object({
        text: Joi.string().min(1).max(30).optional(),
        limit: Joi.number().integer().min(1).max(50).optional(),
        offset: Joi.number().integer().min(0).max(100).optional(),
      }),
    });
  }

  //GET /nodes/{uuid}/homeposts?limit=30
  static homepost() {
    return validate({
      params: Joi.object({
        uuid: Joi.string().min(20).max(40).regex(/^U\w{20,40}/).trim().strict().required()
      }),
      query: Joi.object({
        limit: Joi.number().integer().min(5).max(50).optional(),
      }),
    });
  }

  //PUT /nodes/{uuid} 
  static update() {
    return validate({
      params: Joi.object({
        uuid: Joi.string().min(20).max(40).regex(/^U\w{20,40}/).trim().strict().required()
      }),
      body: Joi.object({
        priority: Joi.number().integer().min(0).max(5),
        followers: Joi.number().integer().min(0).max(100000000),
        following: Joi.number().integer().min(0).max(100000000),
        name: Joi.string().regex(/^[a-zA-Z0-9]+$/).trim().strict()
      })
    });
  }

  //DELETE /nodes/{uuid} 
  static delete() {
    return validate({
      params: Joi.object({
        uuid: Joi.string().min(20).max(40).regex(/^U\w{20,40}/).trim().strict().required()
      })
    });
  }

  //DELETE /relations/{uuid}/following/{toUUID} 
  static unFollow() {
    return validate({
      params: Joi.object({
        uuid: Joi.string().min(20).max(40).regex(/^U\w{20,40}/).trim().strict().required(),
        toUUID: Joi.string().disallow(Joi.ref('uuid')).min(20).max(40).regex(/^U\w{20,40}/).trim().strict().required()
      })
    });
  }
};
