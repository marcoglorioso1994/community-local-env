
const ignitor = require('ignitor.js');

module.exports = ignitor.Model('community', {
    UUID: { type: 'string', required: true },
    email: { type: 'string', required: true },
    name: { type: 'string', required: true },
    priority: { type: 'number', required: true },
    followers: { type: 'number', required: true },
    following: { type: 'number', required: true },
    totalLike: { type: 'number', required: true },
    totalComments: { type: 'number', required: true },
    totalClicksOnUrls: { type: 'number', required: true },
});