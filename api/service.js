const arangojs = require("arangojs");
const aql = arangojs.aql;
const { db } = require("../config/dbConfig");
const Set = require("collections/set");
const HelperPriority = require("../utils/helperPriority");
const { getAudiencePosts } = require("../utils/helperHttp");
const { getFirestoreProfiles } = require("../utils/helperFirebase");

module.exports = class Service {

  //Nodes service
  static async create(body) {

    db.useDatabase("community");
    const col = db.collection("nodes");

    const user = {
      _key: body.uuid,
      email: body.email,
      name: body.name,
      priority: body.priority ? body.priority : 0,
      followers: 0,
      following: 0,
      totalLikes: 0,
      totalComments: 0,
      totalClicksOnUrls: 0,
      lastPriorityUpdate: Math.floor(Date.now() / 1000),
    };

    let meta = await col.save(user, {
      returnNew: true,
      overwrite: false
    });

    return meta.new;
  }

  static async getFollowing(uuid) {

    db.useDatabase("community");

    const cursor = await db.query(
      `FOR r IN relations
          FILTER r._from == @from
          RETURN r._to`,
      { from: `nodes/${uuid}` }
    );

    let result = [];

    while (cursor.hasNext()) {
      const val = await cursor.next();
      result.push(val.substring(6));
    }
    return result;
  }

  static async getFollowers(uuid) {

    db.useDatabase("community");

    const cursor = await db.query(
      `FOR r IN relations
          FILTER r._to == @to
          RETURN r._from`,
      { to: `nodes/${uuid}` }
    );

    let result = [];

    while (cursor.hasNext()) {
      const val = await cursor.next();
      result.push(val.substring(6));
    }
    return result;
  }

  static async isFollowing(uuid, toUUID) {

    db.useDatabase("community");

    const cursor = await db.query(
      `FOR r IN relations
          FILTER r._from == @from AND r._to == @to
          RETURN r`,
      { from: `nodes/${uuid}`, to: `nodes/${toUUID}` }
    );

    if (cursor.hasNext()) {
      return true;
    } else {
      return false;
    }
  }

  /**
   * Used to update the value of priority
   * 
   * @param {*} uuid 
   * @param {*} body 
   * @returns 
   */
  static async updateUser(uuid, body) {

    db.useDatabase("community");

    const result = await db.query(
      ` UPDATE @key WITH @body IN nodes
        RETURN NEW`
      , { "key": uuid, "body": body }
    );

    return result["_result"][0];
  }

  static async deleteUser(uuid) {

    db.useDatabase("community");

    //List of the node's relations
    const nodeEdges = await db.query(
      `FOR r IN relations
          FILTER r._from == @uuid OR r._to == @uuid
          RETURN r`,
      { uuid: `nodes/${uuid}` }
    );

    //Update of the value of followers and following for the neighbord edges
    while (nodeEdges.hasNext()) {
      const val = await nodeEdges.next();

      if (val._from === `nodes/${uuid}`) {
        //Outbound edge _from==nodes/${uuid} 
        await db.query(
          `LET doc = DOCUMENT(@node)
              UPDATE doc._key WITH {followers: doc.followers - 1} IN nodes`,
          { node: val._to }
        );
      } else {
        //Inbound edge _to==nodes/${uuid} 
        await db.query(
          `LET doc = DOCUMENT(@node)
              UPDATE doc._key WITH {following: doc.following - 1} IN nodes`,
          { node: val._from }
        );
      }
    }

    //Remove the node and its relations
    const result = await db.query(`
            LET edgeKeys = (FOR v, e IN 1..1 ANY @el GRAPH @graph RETURN e._key)
            LET r = (FOR key IN edgeKeys REMOVE key IN relations)
            REMOVE @node IN nodes RETURN OLD`,
      { node: uuid, graph: 'community', el: `nodes/${uuid}` }
    )

    return result;
  }

  /**
  * Search node name using ArangoSearch and LIKE operator
  * 
  * @param {*} uuid 
  * @param {*} text 
  * @param {*} limit 
  * @returns an array of firebase profiles
  */
  static async search(uuid, text, offset, limit) {

    db.useDatabase("community");

    var cursor
    var results = new Map();

    if (text === "") {
      //Empty text query
      cursor = await db.query(
        aql`FOR node IN nodes
              SORT node.priority DESC
              LIMIT ${offset}, ${limit}
              RETURN node`
      );

      while (cursor.hasNext()) {
        let user = await cursor.next();
        results.set(user._key, user)
      }

      return Array.from(results.values())

    } else {
      //Influencer user query
      cursor = await db.query(
        aql`LET words = TOKENS(${text},'text_en')
            FOR node IN nodes_view
              SEARCH ANALYZER(node.name IN words,'text_en')
              FILTER node.priority == 4 OR node.priority == 5
              SORT BM25(node) DESC
              LIMIT ${offset}, ${limit}
              RETURN node`
      );

      while (cursor.hasNext()) {
        let user = await cursor.next();
        results.set(user._key, user)
        limit--
      }

      //Arangodb regular expression. Characthers before and after
      var rule = `\%${text}\%`

      if (limit > 0) {
        //Following user query
        cursor = await db.query(
          aql`FOR node IN nodes
                FILTER LIKE (node.name, ${rule}, true)
                LET followingEdges = (
                    FOR vertex, edge IN 1..1 INBOUND node GRAPH community
                      FILTER vertex._key == ${uuid}
                      RETURN edge
                )
                FILTER COUNT(followingEdges) > 0
                SORT node.priority DESC
                LIMIT ${offset}, ${limit}
                RETURN node`
        );

        while (cursor.hasNext()) {
          let user = await cursor.next();
          results.set(user._key, user)
          limit--
        }
      }

      if (limit > 0) {
        //Unknow user query
        cursor = await db.query(
          aql`FOR node IN nodes
                FILTER LIKE(node.name, ${rule}, true)
                SORT node.priority DESC
                LIMIT ${offset}, ${limit}
                RETURN node`
        );

        while (cursor.hasNext()) {
          let user = await cursor.next();
          results.set(user._key, user)
          limit--
        }
      }

      var profiles = await getFirestoreProfiles(Array.from(results.values()))
      return profiles
    }
  }

  /**
  * Homepost function return an array of nodes _key values to populate the homeposts
  * 
  * @param {*} limit 
  * @returns return an array of string uuid
  */
  static async homepost(uuid, limit) {

    db.useDatabase("community");

    var cursor
    var results = new Set();

    var even = 2 * Math.round(limit / 2);
    var half = even / 2

    //Influencer nodes
    cursor = await db.query(
      aql`FOR node IN nodes
            FILTER node.priority == 4 OR node.priority == 5
            FILTER node._key != ${uuid}
            SORT RAND()
            LIMIT ${half}
            RETURN node._key`
    );

    while (cursor.hasNext()) {
      let key = await cursor.next();
      results.add(key)
    }

    //Following user query
    cursor = await db.query(
      aql`FOR node IN nodes
              LET followingEdges = (
                  FOR vertex, edge IN 1..1 INBOUND node GRAPH community
                    FILTER vertex._key == ${uuid}
                    RETURN edge
              )
              FILTER COUNT(followingEdges) > 0
              SORT RAND()
              LIMIT ${half}
              RETURN node._key`
    );

    while (cursor.hasNext()) {
      let key = await cursor.next();
      results.add(key)
    }


    //Check to return a full array
    if (results.length < limit) {

      let gap = limit - results.length
      //Unknow user query
      cursor = await db.query(
        aql`FOR node IN nodes
              SORT RAND()
              LIMIT ${gap}
              RETURN node._key`
      );

      while (cursor.hasNext()) {
        let key = await cursor.next();
        results.add(key)
      }
    }

    return Array.from(results);
  }

  //Relations service
  static async follow(uuid, toUUID) {

    db.useDatabase("community");

    //Validation to check if the both nodes are existing
    let result = await this.isValidPairNodes(uuid, toUUID)

    if (result) {

      const cursor = await db.query(
        `FOR r IN relations 
          FILTER r._from == @from AND r._to == @to 
          LIMIT 1 
          RETURN r`,
        { from: `nodes/${uuid}`, to: `nodes/${toUUID}` }
      );

      if (cursor.hasNext()) {
        //Relation doc already exist
        return {
          'new': false,
          'relation': await cursor.next()
        }

      } else {

        const collection = db.collection("relations");
        //New relation doc
        const relation = {
          _from: `nodes/${uuid}`,
          _to: `nodes/${toUUID}`,
        };

        const meta = await collection.save(relation, { returnNew: true });

        await db.query(
          `LET doc = DOCUMENT(@node)
          UPDATE doc._key WITH {following: doc.following + 1} IN nodes`,
          { node: `nodes/${uuid}` }
        );

        await db.query(
          `LET doc = DOCUMENT(@node)
          UPDATE doc._key WITH {followers: doc.followers + 1} IN nodes`,
          { node: `nodes/${toUUID}` }
        );

        this.isPriorityChanged(uuid);
        this.isPriorityChanged(toUUID);

        return {
          'new': true,
          'relation': meta.new
        }
      }

    } else {
      throw new Error("Nodes not exist")
    }
  }

  static async unFollow(uuid, toUUID) {

    db.useDatabase("community");

    //Validation to check if the both nodes are existing
    var result = await this.isValidPairNodes(uuid, toUUID)

    if (result) {

      const cursor = await db.query(
        `FOR r IN relations
        FILTER r._from == @from AND r._to == @to
        REMOVE { _key: r._key } IN relations RETURN OLD`,
        { from: `nodes/${uuid}`, to: `nodes/${toUUID}` }
      );

      if (cursor.hasNext()) {
        //Update the start node
        await db.query(
          `LET doc = DOCUMENT(@node)
          UPDATE doc._key WITH {following: doc.following - 1} IN nodes`,
          { node: `nodes/${uuid}` }
        );
        //Update the end node
        await db.query(
          `LET doc = DOCUMENT(@node)
          UPDATE doc._key WITH {followers: doc.followers - 1} IN nodes`,
          { node: `nodes/${toUUID}` }
        );

        //The removed edge
        return await cursor.next()

      } else {
        return null
      }

    } else {
      throw new Error("Nodes not exist")
    }
  }

  static async isPriorityChanged(uuid) {
    try {
      db.useDatabase("community");

      const result = await db.query(
        `For n IN nodes
          FILTER n._key == @uuid
          LET seconds = DATE_NOW() / 1000
          FILTER DATE_DIFF(n.lastPriorityUpdate, seconds, "days", false) >= 7
          RETURN n`
        , { uuid: uuid }
      )

      if (result.hasNext()) {
        const { priority, followers, following } = await result.next();

        if (priority < 4) {
          const res = await getAudiencePosts(uuid)

          if (res != null) {
            const audience = res.audience

            const actualPriority = HelperPriority.findPriority(
              followers,
              following,
              audience.totalLikes,
              audience.totalComments
            );

            if (actualPriority != priority) {
              //Arango query to update the values of priority
              await this.updateUser(uuid, {
                priority: actualPriority,
                lastPriorityUpdate: Math.floor(Date.now() / 1000),
              });

              return true
            }
          }
        }
      }

      return false

    } catch (error) {
      console.error(error);
    }
  }

  static async isValidPairNodes(uuid, toUUID) {

    var result = await db.query(
      `RETURN LENGTH(FOR d IN nodes FILTER d._key == @uuid LIMIT 1 RETURN true) > 0`,
      { uuid: uuid }
    );

    if (result._result[0]) {
      result = await db.query(
        `RETURN LENGTH(FOR d IN nodes FILTER d._key == @uuid LIMIT 1 RETURN true) > 0`,
        { uuid: toUUID }
      );

      if (result._result[0]) { return true }
    }

    return false
  }
};
