const express = require("express");
const router = express.Router();

const ControllerNodes = require("./controllerNodes");
const Validator = require("./validator");

router.post("/",
  Validator.create(),
  ControllerNodes.create
);

router.get("/:uuid/following",
  Validator.getFollowing(),
  ControllerNodes.getFollowing
);

router.get("/:uuid/followers",
  Validator.getFollowers(),
  ControllerNodes.getFollowers
);

router.get("/:uuid/following/:toUUID",
  Validator.isFollowing(),
  ControllerNodes.isFollowing
);

//Need new implementation
router.get("/:uuid/search",
  Validator.search(),
  ControllerNodes.search
);

//Not implemented
router.get("/:uuid/homepost",
  Validator.homepost(),
  ControllerNodes.homepost
);

router.put("/:uuid",
  Validator.update(),
  ControllerNodes.update
);

router.delete("/:uuid",
  Validator.delete(),
  ControllerNodes.delete
);

module.exports = router;
