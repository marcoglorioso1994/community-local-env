const express = require("express");
const router = express.Router();

const nodesAPI = require("./routerNodes");
const relationsAPI = require("./routerRelations");

router.use("/nodes", nodesAPI);

router.use("/relations", relationsAPI);

module.exports = router;
