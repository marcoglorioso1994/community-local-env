const Service = require("./service");

const DEFAULT_LIMIT = 20
const DEFAULT_OFFSET = 0

module.exports = class controllerNodes {
  static async create(req, res) {

    try {

      const { body } = req;
      const newNode = await Service.create(body);

      res.json({ code: 200, inserted: 1, node: newNode });
      return

    } catch (error) {
      console.error(error.stack);
      res.status(500).send({ error: error.message });
    }
  }

  static async getFollowing(req, res) {

    try {

      const { uuid } = req.params;
      const nodes = await Service.getFollowing(uuid);

      res.json({ code: 200, following: nodes });
      return

    } catch (error) {
      console.error(error.stack);
      res.status(500).send({ error: error.message });
    }
  }

  static async getFollowers(req, res) {

    try {

      const { uuid } = req.params;
      const nodes = await Service.getFollowers(uuid);

      res.json({ code: 200, followers: nodes });
      return

    } catch (error) {
      console.error(error.stack);
      res.status(500).send({ error: error.message });
    }
  }

  static async isFollowing(req, res) {

    try {
      const { uuid, toUUID } = req.params;
      const isFollowing = await Service.isFollowing(uuid, toUUID);

      res.json({ code: 200, from: uuid, to: toUUID, relationFollowing: isFollowing });
      return

    } catch (error) {
      console.error(error.stack);
      res.status(500).send({ error: error.message });
    }
  }

  static async update(req, res) {

    try {
      const { uuid } = req.params;
      const { body } = req;

      const newNode = await Service.updateUser(uuid, body);

      res.json({ code: 200, message: `Successfully updated node ${uuid}`, node: newNode });
      return

    } catch (error) {
      console.error(error.stack);
      res.status(500).send({ error: error.message });
    }
  }

  static async delete(req, res) {

    try {

      const { uuid } = req.params;
      await Service.deleteUser(uuid);

      res.json({ code: 200, removed: 1, message: `Successfully removed node ${uuid}` });
      return

    } catch (error) {
      console.error(error.stack);
      res.status(500).send({ error: error.message });
    }
  }

  static async search(req, res) {

    try {
      const { uuid } = req.params;
      var { text, offset, limit } = req.query;

      //text query param validation
      if (text == undefined) {
        text = ""
      } else {
        text = text.toLowerCase();
      }

      //offset query param validation
      if (offset == undefined || offset == "") {
        offset = DEFAULT_OFFSET
      } else {
        offset = parseInt(offset)
      }

      //limit query param validation
      if (limit == undefined || limit == "") {
        limit = DEFAULT_LIMIT
      } else {
        limit = parseInt(limit)
      }

      const profiles = await Service.search(uuid, text, offset, limit);

      res.json({ code: 200, profiles: profiles });
      return

    } catch (error) {
      console.error(error.stack);
      res.status(500).send({ error: error.message });
    }
  }

  static async homepost(req, res) {

    try {
      const { uuid } = req.params;
      var { limit } = req.query;

      //limit query param validation
      if (limit == undefined || limit == "") {
        limit = DEFAULT_LIMIT
      } else {
        limit = parseInt(limit)
      }

      const uuids = await Service.homepost(uuid, limit);

      res.json({ code: 200, results: uuids });
      return

    } catch (error) {
      console.error(error.stack);
      res.status(500).send({ error: error.message });
    }
  }
};
