const Service = require("./service");

module.exports = class ControllerRealtions {

    static async follow(req, res) {

        try {
            const { uuid, toUUID } = req.body;
            const response = await Service.follow(uuid, toUUID);

            var inserted = 0

            if (response.new) {
                inserted++
            }

            res.json({ code: 200, inserted: inserted, relation: response.relation });
            return

        } catch (error) {
            console.error(error.stack);
            res.status(500).send({ error: error.message });
        }
    }

    static async unFollow(req, res) {

        try {
            const { uuid, toUUID } = req.params;
            const oldEdge = await Service.unFollow(uuid, toUUID);

            if (oldEdge == null) {
                res.status(404).send({ error: `Not found a relation from ${uuid} to ${toUUID}` });
                return
            }
            res.json({ code: 200, removed: 1, message: `Successfully removed relation from ${uuid} to ${toUUID}` });
            return

        } catch (error) {
            console.error(error.stack);
            res.status(500).send({ error: error.message });
        }
    }
}